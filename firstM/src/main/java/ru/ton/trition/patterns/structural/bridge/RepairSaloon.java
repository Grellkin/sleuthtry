package ru.ton.trition.patterns.structural.bridge;

import ru.ton.trition.patterns.structural.bridge.car_hirarchy.Car;

public class RepairSaloon {

    private Car donorCar;

    public void setDonorCar(Car parkedCar) {
        this.donorCar = parkedCar;
    }

    public void fixCar(Car car) {
        if (!car.getEngine().turnOn()) {
            car.setEngine(donorCar.getEngine());
        }
        if (!car.getTransmission().connectEngineAndWheels()) {
            car.setTransmission(donorCar.getTransmission());
        }

    }
}
