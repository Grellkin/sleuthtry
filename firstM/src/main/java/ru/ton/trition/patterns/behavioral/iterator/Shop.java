package ru.ton.trition.patterns.behavioral.iterator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.List;

public class Shop {

    EnumMap<ItemCategory, List<Item>> itemMap = new EnumMap<>(ItemCategory.class);

    {
        Arrays.stream(ItemCategory.values()).forEach(v -> itemMap.put(v, new ArrayList<>()));
    }

    public Iterator<Item> iterator(ItemCategory category) {
        return itemMap.get(category).iterator();
    }


}
