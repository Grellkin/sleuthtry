package ru.ton.trition.patterns.creational.factory_method.factory;

import ru.ton.trition.patterns.creational.factory_method.product.Coconut;
import ru.ton.trition.patterns.creational.factory_method.product.Nut;

public class CoconutPalm extends NutTree {
    public CoconutPalm(int height, int age) {
        super(height, age);
    }

    @Override
    public void grow() {
        final int newHeight = getHeight() + getAge() * 2;
        setHeight(newHeight);
    }

    @Override
    public Nut produceNut() {
        return new Coconut(1, "jimi", "west india");
    }
}
