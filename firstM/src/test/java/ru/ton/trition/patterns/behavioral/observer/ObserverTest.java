package ru.ton.trition.patterns.behavioral.observer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ObserverTest {

    @Test
    public void testUser() {
        SummonerObserver observer = new SummonerObserver();
        NotifiableDemon demon = new Demon();
        NotifiableDemon hellDog = new HellDog();

        assertFalse(demon.isSummoned());
        assertFalse(hellDog.isSummoned());
        observer.subscribeToAction(Cast.DEMON_SUMMON, demon);
        observer.subscribeToAction(Cast.HELLDOG_SUMMON, demon);

        observer.simulateEvent(Cast.DEMON_SUMMON);
        assertTrue(demon.isSummoned());
        assertFalse(hellDog.isSummoned());
        observer.unsubscribeFromAllEvents(demon);


    }

}