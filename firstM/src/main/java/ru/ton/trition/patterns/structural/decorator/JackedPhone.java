package ru.ton.trition.patterns.structural.decorator;

public abstract class JackedPhone implements CellPhone {

    protected CellPhone cellPhone;

    public JackedPhone(CellPhone cellPhone) {
        this.cellPhone = cellPhone;
    }

    public abstract void defendFromHit();

}
