package ru.ton.trition.patterns.behavioral.template;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    public void testUser() {
        Person person = new Man();
        Person person1 = new Woman();
        person.liveDecentLife();
        person1.liveDecentLife();

        Assertions.assertTrue(person1.getAge() > person.getAge());
        Assertions.assertEquals("served in army", person.getAchievement());
        Assertions.assertEquals("born a child", person1.getAchievement());

    }

}