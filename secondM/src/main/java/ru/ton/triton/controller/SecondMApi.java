package ru.ton.triton.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ru.ton.triton.Retro;
import ru.ton.triton.service.SecondMService;

import java.time.LocalDateTime;
import java.util.Date;

@RestController
@Slf4j
@RequestMapping("second/hello/world")
@RequiredArgsConstructor
public class SecondMApi {

    private final SecondMService secondMService;

    @GetMapping
    public String helloWorld() {
        log.info("Hello, it`s me. From secondM hello world.");
        final String s = secondMService.firstCall();
        log.info("Bye with response s! {}", s);
        return new Date().toString();
    }

    @GetMapping("/shalom/bruda")
    public Retro helloWorld1(@RequestParam(name = "page") Integer page) {
        log.info("Hello, it`s me. From secondM here I come page = {}.", page);
        return new Retro("string one", new Date(), page);
    }

    @PostMapping("/shalom/bruda")
    public String helloWorld2(@RequestBody Retro retro) {
        log.info("Hello, it`s me. From secondM here I receive post method with retro {}.", retro);
        return "pobeda nashih nad retrofitom";
    }
}

