package ru.ton.trition.patterns.structural.bridge.car_hirarchy;

import lombok.Getter;
import lombok.Setter;
import ru.ton.trition.patterns.structural.bridge.engine_hirarchy.Engine;
import ru.ton.trition.patterns.structural.bridge.transmission_hierarchy.Transmission;

@Setter
@Getter
public abstract class Car {

    protected Engine engine;
    protected Transmission transmission;
    protected int amountOfPetrol;

    public Car(Engine engine, Transmission transmission) {
        this.engine = engine;
        this.transmission = transmission;
    }

    public abstract void move();
}
