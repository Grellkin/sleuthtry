package ru.ton.trition.patterns.behavioral.template;

public class Woman extends Person {

    @Override
    protected void dieTemplate() {
         age += 35;
    }

    @Override
    protected void makeTeplateAction() {
        achievement = "born a child";
    }
}
