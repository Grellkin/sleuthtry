package ru.ton.trition.patterns.behavioral.observer;

public abstract class NotifiableDemon implements NotifyTarget {

    protected boolean summoned;

    public boolean isSummoned() {
        return summoned;
    }

    //override equals and hashcode here, must be done for list.remove()
}
