package ru.ton.trition.patterns.creational.factory_method.product;

public class GreeceWalnut extends Walnut {
    public GreeceWalnut(int size, String name, String origin) {
        super(size, name, origin);
    }

    @Override
    protected String getKind() {
        return "greece";
    }
}
