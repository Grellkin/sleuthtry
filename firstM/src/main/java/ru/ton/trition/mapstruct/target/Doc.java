package ru.ton.trition.mapstruct.target;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Builder
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Doc {

    private int age;
    private Auth auth;
    private List<Pag> pages;
    @Setter
    private String king;
    private Map<String, Integer> docMap;
    private Map<String, Integer> docMapForTest;
}
