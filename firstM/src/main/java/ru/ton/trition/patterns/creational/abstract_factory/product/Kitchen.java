package ru.ton.trition.patterns.creational.abstract_factory.product;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public class Kitchen {

    private Fridge fridge;
    private Owen owen;

}
