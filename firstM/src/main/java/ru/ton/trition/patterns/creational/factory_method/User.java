package ru.ton.trition.patterns.creational.factory_method;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.stereotype.Component;
import ru.ton.trition.patterns.creational.factory_method.factory.NutTree;
import ru.ton.trition.patterns.creational.factory_method.factory.WalnutPalm;
import ru.ton.trition.patterns.creational.factory_method.factory_2.Bush;
import ru.ton.trition.patterns.creational.factory_method.product.Nut;

import javax.annotation.PostConstruct;

@Component
public class User {

    @PostConstruct
    public void justUse() {
        //first way of factory, GOF
        NutTree tree = new WalnutPalm(12, 2, "greece");
        useAfterReceive(tree);
        //second way of factory, colhos
        Bush bush = new Bush("greece");
        useAfterReceive(bush);
    }

    private void useAfterReceive(Bush bush) {
        final Nut walnut = bush.growNut("walnut");
        System.out.println("Eat_2 nut with nutrition " + walnut.getNutrition());
    }

    public void useAfterReceive(NutTree tree) {
        tree.getOlder();
        //we do not suppose to use by ourself, gof`s vision of factory method intend to self use of method by
        //class holder.
        // Look at abstract factory example: designer use it`s own abstract methods, here we suppose to do same
        final Nut nut = tree.produceNut();
        final int nutrition = nut.getNutrition();
        System.out.println("Eat nut with nutrition " + nutrition);
    }
}
