package ru.ton.trition.mapstruct;

import org.mapstruct.Qualifier;
import org.springframework.stereotype.Component;
import ru.ton.trition.mapstruct.source.Document;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class HelpMapper {


    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface Name {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public @interface Author {
    }

    @Name
    public Map<String, Integer> mapDocumentToNameMap(Document document) {
        Map<String, Integer> result = new HashMap<>();
        result.put(document.getName(), 333);
        return result;
    }

    @Author
    public Map<String, Integer> mapDocumentToAuthorMap(Document document) {
        return Collections.singletonMap(document.getAuthor().getName(), 1488);
    }
}
