package ru.ton.trition.patterns.structural.facade.original;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.ton.trition.patterns.structural.facade.facade.DbReaderPanel;

class UserTest {

    @Test
    public void testUser() {
        Db db = new Db("oracle");
        //facades
        DbReaderPanel dbPanel = new DbReaderPanel(db);
        DbAdminPanel dbAPanel = new DbAdminPanel(db);

        UserInterface ui = new UserInterface();
        ui.setPanel(dbPanel);
        Assertions.assertDoesNotThrow(ui::readInfo);
        Assertions.assertThrows(RuntimeException.class, () -> ui.writeInfo("privet"));

        ui.setPanel(dbAPanel);
        Assertions.assertDoesNotThrow(ui::readInfo);
        Assertions.assertDoesNotThrow(() -> ui.writeInfo("privet"));
    }

}