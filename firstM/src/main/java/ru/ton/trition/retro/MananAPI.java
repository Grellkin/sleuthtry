package ru.ton.trition.retro;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.ton.triton.Retro;


public interface MananAPI {

    @GET("shalom/bruda")
    Call<Retro> shalomBruda(@Query("page") int page);

    @POST("shalom/bruda")
    Call<String> shalomBruda(@Body Retro retro);

}
