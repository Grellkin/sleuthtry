package ru.ton.trition.mapstruct;

import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.DecoratedWith;
import org.mapstruct.MapMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import ru.ton.trition.mapstruct.source.Document;
import ru.ton.trition.mapstruct.source.Page;
import ru.ton.trition.mapstruct.source.Text;
import ru.ton.trition.mapstruct.target.Doc;
import ru.ton.trition.mapstruct.target.Pag;
import ru.ton.trition.mapstruct.target.Tex;

@Mapper(componentModel = "spring", uses = HelpMapper.class)
@DecoratedWith(MapStrAbs.class)
public interface MapStr {

    @Mapping(source = "author", target = "auth")
    @Mapping(target = "docMap", qualifiedBy = HelpMapper.Author.class, source = "document")
    @Mapping(target = "docMapForTest", qualifiedBy = HelpMapper.Name.class, source = "document")
    Doc convertDocumentToDoc(Document document);

    @Mapping(source = "idshechka", target = "id")
    @Mapping(target = "uuid", expression = "java(java.util.UUID.randomUUID())")
    Pag pageToPag(Page page);

    default Tex dance(Text text) {
        if (text == null) {
            return null;
        }

        final String header = text.getHeader();

        return Tex.builder()
                .body(text.getBody())
                .footage(text.getFootage())
                .priceOfTex(header == null ? 12 : header.length())
                .build();
    }

     @AfterMapping //builder because using builders, if setters or constructors then just object
     default void fill(@MappingTarget Pag.PagBuilder pag) {
        pag.description("this is the after-mapping description");
    }
}
