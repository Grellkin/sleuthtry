package ru.ton.trition.patterns.creational.abstract_factory.factory;

import ru.ton.trition.patterns.creational.abstract_factory.product.Fridge;
import ru.ton.trition.patterns.creational.abstract_factory.product.Kitchen;
import ru.ton.trition.patterns.creational.abstract_factory.product.Owen;

public abstract class KitchenDesignerImpl implements KitchenDesigner {

    @Override
    public Kitchen designNewKitchen() {
        return new Kitchen(createFridge(), createOwen());
    }

    protected abstract Owen createOwen();

    protected abstract Fridge createFridge();
}
