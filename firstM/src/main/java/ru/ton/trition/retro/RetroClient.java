package ru.ton.trition.retro;

import ru.ton.triton.Retro;

public class RetroClient implements MananFeignAPI, RetroFitApi {

    private MananAPI api;

    public void setBaseUrl(String url) {
        final MananAPI mananAPI = createApi(url).create(MananAPI.class);
        this.api = mananAPI;
    }

    @Override
    public Retro shalomBruda(int page) {
        return callMethod(api.shalomBruda(page));
    }

    @Override
    public String shalomBruda(Retro retro) {
        return callMethod(api.shalomBruda(retro));
    }


}
