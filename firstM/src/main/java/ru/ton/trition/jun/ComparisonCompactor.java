package ru.ton.trition.jun;

import junit.framework.Assert;
import org.jfree.date.SerialDate;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ComparisonCompactor {

    private static final String ELLIPSIS = "...";

    private int contextLength;
    private String expected;
    private String actual;

    private int prefixLength;
    private int suffixLength;

    public ComparisonCompactor(int contextLength,
                               String expected,
                               String actual) {
        this.contextLength = contextLength;
        this.expected = expected;
        this.actual = actual;
    }
    public String compact(String message) {


        if (compacted()) {
            compactExpectedAndActual();
        }
        return Assert.format(message, expected, actual);
    }

    private boolean compacted() {
        return !(expected == null || actual == null || areStringsEqual());
    }

    private void compactExpectedAndActual() {
        findCommonPrefix();
        findCommonSuffix();
        format();
    }

    private void findCommonPrefix() {
        for (prefixLength = 0; isPrefixLessThenWords(); prefixLength++) {
            if (expected.charAt(prefixLength) != actual.charAt(prefixLength))
                break;
        }
    }

    private boolean isPrefixLessThenWords() {
        return prefixLength < expected.length() && prefixLength < actual.length();
    }

    private void findCommonSuffix() {
        for (suffixLength = 0; suffixNotOverlapsPrefix(); suffixLength++) {
            if (charFromEnd(expected) != charFromEnd(actual))
                break;
        }
    }

    private char charFromEnd(String word) {
        return word.charAt(word.length() - 1 - suffixLength);
    }


    private boolean suffixNotOverlapsPrefix() {
        return (expected.length() - prefixLength - suffixLength > 0)
                && (actual.length() - prefixLength - suffixLength > 0);
    }

    private void format() {
        String temporaryExpected = formatComparision(expected);
        actual = formatComparision(actual);
        expected = temporaryExpected;
    }

    private String formatComparision(String source) {
        return computeCommonPrefix()
                + "[" +
                source.substring(prefixLength, source.length() - suffixLength)
                + "]"
                + computeCommonSuffix();
    }

    private String computeCommonPrefix() {

        return (prefixLength > contextLength ? ELLIPSIS : "") +
                expected.substring(Math.max(0, prefixLength - contextLength), prefixLength);
    }

    private String computeCommonSuffix() {
        final int contextStart = expected.length() - suffixLength;
        int contextEnd = Math.min(contextStart + contextLength, expected.length());
        return expected.substring(contextStart, contextEnd) + (suffixLength > contextLength ? ELLIPSIS : "");
    }

    private boolean areStringsEqual() {
        return expected.equals(actual);
    }
}
