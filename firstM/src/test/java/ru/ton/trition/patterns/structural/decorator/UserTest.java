package ru.ton.trition.patterns.structural.decorator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserTest {


    @Test
    public void testUser() {
        /*
        Important difference between proxy and decorator is a living cycle of decorated/proxied object,
            usually user has access to original object and to decorated in case of decoration also decorator is able
            to add few own methods, from the other hand proxy guarantee no access to original object for clients and
            carry exact same interface.
        Wrapped object always passed to decorator class, but not the same for proxy. Sometimes proxy has authority to
            create instance by herself or by a part of dependency injection.
         */
        CellPhone phone = new CellPhoneImpl();
        phone.call();

        JackedPhone jackedPhone = new LeatherJackedPhone(phone);

        final String sound = jackedPhone.call();
        jackedPhone.defendFromHit();
        Assertions.assertEquals("Hello, it`s John shena, how are you?", sound);
    }
}