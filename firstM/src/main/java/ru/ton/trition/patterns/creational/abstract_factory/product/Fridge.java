package ru.ton.trition.patterns.creational.abstract_factory.product;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public abstract class Fridge {

    private int size;
    private String country;

}
