package ru.ton.trition.mapstruct.source;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Getter
public class Text {

    private String footage;
    private String body;
    private String header;
}
