package ru.ton.trition.controller;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.ton.trition.mapstruct.MapStr;
import ru.ton.trition.mapstruct.MapStrAbs;
import ru.ton.trition.mapstruct.source.Author;
import ru.ton.trition.mapstruct.source.Document;
import ru.ton.trition.mapstruct.source.Page;
import ru.ton.trition.mapstruct.source.Text;
import ru.ton.trition.mapstruct.target.Doc;
import ru.ton.trition.retro.MananAPI;
import ru.ton.trition.retro.MananFeignAPI;
import ru.ton.trition.retro.RetroClient;
import ru.ton.trition.service.FirstMService;
import ru.ton.trition.shit.Cat;
import ru.ton.trition.shit.CatService;
import ru.ton.triton.Retro;

import java.util.ArrayList;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("first/hello/world")
@RequiredArgsConstructor
public class FirstMApi {

    private final FirstMService firstMService;
    private final MapStr mapStr;

    @GetMapping("/map")
    public Doc maps() {

        Page page1 = Page.builder().number(1).idshechka(99L).text(new Text("bottom", "middle", "head")).build();
        Page page2 = Page.builder().number(2).text(new Text("bottom2", "middle2", "head2")).build();
        Page page3 = Page.builder().number(3).text(new Text("bottom3", "middle3", "head3")).build();

        Document document = Document.builder()
                .age(12)
                .author(Author.builder().name("shalom_author").build())
                .name("Passport")
                .pages(List.of(page1, page2, page3))
                .build();

        final Doc doc = mapStr.convertDocumentToDoc(document);
        System.out.println(doc);
        return doc;
    }

    @GetMapping
    public String helloWorld() {
        CatService<String> catService = new CatService<>();
        List<Integer> cats = new ArrayList<>();
        catService.hi(cats);

        log.info("Hello, it`s me. From firstM hello world.");
        final String s = firstMService.firstCall();
        log.info("Bye with response s! {}", s);
        return s;
    }

    @SneakyThrows
    @GetMapping("/getOne")
    public void helloWorld1() {
        log.info("Hello, it`s me. From firstM1 hello world.");
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://localhost:8081/second/hello/world/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(httpClient.build())
//                .build();
//
//        final MananAPI mananAPI = retrofit.create(MananAPI.class);
//        final Call<Retro> retro = mananAPI.shalomBruda(12);
//        final Response<Retro> execute = retro.execute();
//        final Retro body = execute.body();

        final RetroClient retroClient = new RetroClient();
        retroClient.setBaseUrl("http://localhost:8081/second/hello/world/");
        retroClient.shalomBruda(12);
        System.out.println("hi");


        final MananFeignAPI target = Feign.builder()
                .decoder(new GsonDecoder())
                .encoder(new GsonEncoder())
                .target(MananFeignAPI.class, "http://localhost:8081/second/hello/world/");
        final Retro retro1 = target.shalomBruda(22);
        System.out.println("sub");


    }



    @GetMapping("/postOne")
    public void helloWorld2() {
        log.info("Hello, it`s me. From firstM2 hello world.");
        final String s = firstMService.firstCall();
        log.info("Bye with response s! {}", s);
    }

}

