package ru.ton.trition.patterns.creational.factory_method.factory_2;

import lombok.AllArgsConstructor;
import ru.ton.trition.patterns.creational.factory_method.product.Coconut;
import ru.ton.trition.patterns.creational.factory_method.product.GreeceWalnut;
import ru.ton.trition.patterns.creational.factory_method.product.Nut;
import ru.ton.trition.patterns.creational.factory_method.product.SpainWalnut;

@AllArgsConstructor
public class Bush {

    private final String country;

    public Nut growNut(String kind) {
        switch (kind) {
            case "coco":
                return new Coconut(2, "coco", "spain");
            case "walnut":
                if(country.equals("greece")) return new GreeceWalnut(2, "walnut", "greece");
                else return new SpainWalnut(2, "walnut", "spain");
            default:
                return null;
        }

    }
}
