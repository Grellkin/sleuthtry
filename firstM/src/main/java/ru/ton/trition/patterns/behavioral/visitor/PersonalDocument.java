package ru.ton.trition.patterns.behavioral.visitor;

import lombok.Getter;

@Getter
public class PersonalDocument implements Document {

    private String name;
    private String lastName;
    private int age;

    public PersonalDocument(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public void visit(Visitorio visitor) {
        visitor.printInfoAboutPersonalDoc(this);
    }

    @Override
    public boolean checkValidity() {
        return name != null && lastName != null && age != 0;
    }

    @Override
    public void invalidate(String reason) {
        name = null;
    }


}
