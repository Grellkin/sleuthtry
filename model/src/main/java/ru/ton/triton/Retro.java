package ru.ton.triton;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Retro {

    private String firstParam;
    private Date ldt;
    private Integer someMoney;

}
