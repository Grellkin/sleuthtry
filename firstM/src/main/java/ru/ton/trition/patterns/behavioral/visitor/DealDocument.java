package ru.ton.trition.patterns.behavioral.visitor;


import lombok.Getter;

@Getter
public class DealDocument implements Document {

    private String nameSeller;
    private String lastNameSeller;
    private String nameBuyer;
    private String lastNameBuyer;

    public DealDocument(String name_seller, String lastName_seller, String name_buyer, String last_name_buyer) {
        this.nameSeller = name_seller;
        this.lastNameBuyer = last_name_buyer;
        this.nameBuyer = name_buyer;
        this.lastNameSeller = lastName_seller;
    }

    @Override
    public void visit(Visitorio visitor) {
        visitor.printInfoAboutDealDoc(this);
    }

    @Override
    public boolean checkValidity() {
        return nameBuyer != null && nameSeller != null;
    }

    @Override
    public void invalidate(String reason) {
        nameSeller = null;
        nameBuyer = null;
    }
}
