package ru.ton.trition.model;

import lombok.Data;

@Data
public class Dog extends Animal<Dog> {
    private Integer tailLength;

    @Override
    public int subcompare(Dog animal) {
        return tailLength.compareTo(animal.tailLength);
    }

    @Override
    public int compareTo(Dog o) {

        return 0;
    }
}
