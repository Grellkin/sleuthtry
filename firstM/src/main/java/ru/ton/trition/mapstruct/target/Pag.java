package ru.ton.trition.mapstruct.target;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Builder
@Getter
@AllArgsConstructor
public class Pag {

    private Integer number;
    private Tex text;
    private Long id;
    @Setter
    private String description;
    private UUID uuid;
}
