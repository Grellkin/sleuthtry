package ru.ton.trition.patterns.creational.abstract_factory.factory;

import ru.ton.trition.patterns.creational.abstract_factory.product.Fridge;
import ru.ton.trition.patterns.creational.abstract_factory.product.Owen;
import ru.ton.trition.patterns.creational.abstract_factory.product.RussianFridge;
import ru.ton.trition.patterns.creational.abstract_factory.product.RussianOwen;

public class RussianDesigner extends KitchenDesignerImpl {
    @Override
    protected Owen createOwen() {
        return new RussianOwen(1, "Russia");
    }

    @Override
    protected Fridge createFridge() {
        return new RussianFridge(22, "Russa");

    }
}
