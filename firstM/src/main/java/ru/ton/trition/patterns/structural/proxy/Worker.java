package ru.ton.trition.patterns.structural.proxy;

public class Worker implements WorkerApi {

    protected double currentAmountOfMoney = 0.0;
    protected double salary;

    @Override
    public double getSalary() {
        return salary;
    }

    @Override
    public void receivePayment(double money) {
        currentAmountOfMoney += money;
    }

    @Override
    public double exposeAmoutOfMoney() {
        return currentAmountOfMoney;
    }

    @Override
    public void getSick(int days) {
        System.out.println("Sitting sick for " + days + " days, angry!");
    }
}
