package ru.ton.trition.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ChoiceFormat;
import java.text.Format;
import java.text.MessageFormat;

import static java.lang.System.in;

@Service
@Slf4j
@RequiredArgsConstructor
public class FirstMService {

    private final RestTemplate restTemplate;

    public String firstCall() {
        final ResponseEntity<String> forEntity = restTemplate.getForEntity("http://second:81/second/hello/world",
                String.class);
        log.info("Get entity from second service {}", forEntity.getBody());
        return forEntity.getBody();
    }

    //@EventListener(ApplicationReadyEvent.class)
    public void some() {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(in))) {
            System.out.println("Vvedite chto vam nado:\n");
            System.out.println(" 1. Kinder\n 2. Kolbacy\n 3. Dga-dgy");
            String s = reader.readLine();
            int action = Integer.parseInt(s);
            System.out.println("Vvedite skolko vam nado:");
            s =reader.readLine();
            s = s.substring(s.length() - 1, s.length());
            int count = Integer.parseInt(s);

            MessageFormat mf = new MessageFormat("Segonia {0} dolshna {1} manany {2} {3}");

            double[] actionLimits = {1,2,3};
            String[] partsAction = {"celovat","terebit","shpekat"};
            ChoiceFormat actonChoice = new ChoiceFormat(actionLimits, partsAction);

            double[] razLimits = {0,2,5};
            String[] partsRaz = {"{3} raz","{3} raza","{3} raz"};
            ChoiceFormat razChoice = new ChoiceFormat(razLimits, partsRaz);

            Format[] testFormats = {null, actonChoice, null, razChoice};
            mf.setFormats(testFormats);
            Object[] testArgs = {"Kika", action, "Tony", count};

            System.out.println(mf.format(testArgs));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
