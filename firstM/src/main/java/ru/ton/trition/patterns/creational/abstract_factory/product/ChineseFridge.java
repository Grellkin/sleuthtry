package ru.ton.trition.patterns.creational.abstract_factory.product;

public class ChineseFridge extends Fridge {

    public ChineseFridge(int size, String country) {
        super(size, country);
    }

}
