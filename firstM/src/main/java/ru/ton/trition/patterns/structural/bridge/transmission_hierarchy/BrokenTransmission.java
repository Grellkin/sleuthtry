package ru.ton.trition.patterns.structural.bridge.transmission_hierarchy;

public class BrokenTransmission implements Transmission {
    @Override
    public boolean connectEngineAndWheels() {
        return false;
    }
}
