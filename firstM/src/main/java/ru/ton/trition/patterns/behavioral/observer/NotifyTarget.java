package ru.ton.trition.patterns.behavioral.observer;

public interface NotifyTarget {

    void notifyMe(Cast eventType);
}
