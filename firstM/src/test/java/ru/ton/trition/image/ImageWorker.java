package ru.ton.trition.image;

import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import javax.imageio.ImageTypeSpecifier;
import javax.imageio.ImageWriter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.bouncycastle.asn1.gnu.GNUObjectIdentifiers.CRC;

public class ImageWorker {

    @Test
    public void work() {
        try {
            final InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("images/dog.jpg");
            final BufferedImage read = ImageIO.read(resourceAsStream);


            final Path file = Files.createFile(Path.of("newdog.png"));


            final int height = read.getHeight();
            final int width = read.getWidth();

            BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
            final Graphics2D graphics = newImage.createGraphics();
            graphics.drawImage(read, 0,0,null);
            graphics.dispose();
//            for (int y = 0; y < height; y++) {
//                for (int x = 0; x < width; x++) {
//
//                }
//            }
            ImageIO.write(newImage, "png", file.toFile());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
