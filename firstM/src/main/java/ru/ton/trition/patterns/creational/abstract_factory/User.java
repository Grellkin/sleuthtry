package ru.ton.trition.patterns.creational.abstract_factory;

import org.springframework.stereotype.Component;
import ru.ton.trition.patterns.creational.abstract_factory.factory.ChineseDesigner;
import ru.ton.trition.patterns.creational.abstract_factory.factory.KitchenDesigner;
import ru.ton.trition.patterns.creational.abstract_factory.factory.RussianDesigner;
import ru.ton.trition.patterns.creational.abstract_factory.product.Kitchen;

import javax.annotation.PostConstruct;
import java.time.LocalDate;

@Component(value = "salamandra")
public class User {

    @PostConstruct
    public void simpleUse() {
        //technically it may be some factory-factory class
        KitchenDesigner designer;
        if (LocalDate.now().getDayOfWeek().getValue() % 2 == 0) {
            designer = new RussianDesigner();
        } else {
            designer = new ChineseDesigner();
        }
        useAfterReceiveFactory(designer);
    }

    public void useAfterReceiveFactory(KitchenDesigner designer) {
        final Kitchen kitchen = designer.designNewKitchen();
        System.out.println("New kitchen designed, see what we got: " + kitchen.toString());
    }
}
