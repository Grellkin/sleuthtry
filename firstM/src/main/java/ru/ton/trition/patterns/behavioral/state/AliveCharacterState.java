package ru.ton.trition.patterns.behavioral.state;

public class AliveCharacterState extends AbsCharacterState {

    public AliveCharacterState(int point, int health, CharacterStateMachine machine) {
        super(point, health, machine);
    }

    @Override
    public int move() {
        return (this.point += 10);
    }

    @Override
    public String say(String phrase) {
        return "ARRRRR " + phrase;
    }

    @Override
    public void deal(int damage) {
        System.out.println("Hit my enemy! With " + damage + " of damage!");
    }

    @Override
    public void receive(int damage) {
        this.health -= damage;
        if (this.health < 0) {
            this.machine.setState(new DeadCharacterState(point, 0, machine));
        }
    }
}
