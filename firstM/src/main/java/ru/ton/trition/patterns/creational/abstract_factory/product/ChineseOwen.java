package ru.ton.trition.patterns.creational.abstract_factory.product;

public class ChineseOwen extends Owen {
    public ChineseOwen(double price, String country) {
        super(price, country);
    }
}
