package ru.ton.trition.patterns.structural.bridge.engine_hirarchy;

public class BrokenEngine implements Engine {
    @Override
    public boolean turnOn() {
        return false;
    }

    @Override
    public void makeSound() {
        System.out.println("mmmmmm mmmmm");
    }
}
