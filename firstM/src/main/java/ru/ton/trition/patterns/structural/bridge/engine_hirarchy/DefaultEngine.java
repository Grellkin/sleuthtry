package ru.ton.trition.patterns.structural.bridge.engine_hirarchy;

public class DefaultEngine implements Engine {

    @Override
    public boolean turnOn() {
        return true;
    }

    @Override
    public void makeSound() {
        System.out.println("wruuuum wruuum");;
    }
}
