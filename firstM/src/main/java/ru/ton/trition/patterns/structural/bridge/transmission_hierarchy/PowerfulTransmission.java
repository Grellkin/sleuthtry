package ru.ton.trition.patterns.structural.bridge.transmission_hierarchy;

public class PowerfulTransmission implements Transmission {
    @Override
    public boolean connectEngineAndWheels() {
        return true;
    }
}
