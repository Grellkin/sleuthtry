package ru.ton.trition.patterns.structural.decorator;

public class LeatherJackedPhone extends JackedPhone {
    public LeatherJackedPhone(CellPhone phone) {
        super(phone);
    }

    @Override
    public void defendFromHit() {
        System.out.println("Leather protects you!");
    }

    @Override
    public String call() {
        final String phraseFromPhone = cellPhone.call();
        return makeSoundLittleWorse(phraseFromPhone);
    }

    private String makeSoundLittleWorse(String sound) {
        return sound.replace("c", "sh");
    }

    @Override
    public void sendSms(String sms) {
        cellPhone.sendSms(sms);
    }
}
