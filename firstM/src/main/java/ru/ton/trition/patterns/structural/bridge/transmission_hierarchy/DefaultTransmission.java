package ru.ton.trition.patterns.structural.bridge.transmission_hierarchy;

public class DefaultTransmission implements Transmission {
    @Override
    public boolean connectEngineAndWheels() {
        return true;
    }
}
