package ru.ton.trition.patterns.structural.facade.original;

public class Db {

    private String type;

    public Db(String oracle) {
        this.type = oracle;
    }

    public String read() {
        return "db data";
    }

    public void write(byte[] data) {
        System.out.println("writing bytes to db " + data);
    }
}
