package ru.ton.trition.retro;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
public interface RetroFitApi {

    default <T> T callMethod(Call<T> t) {
        try {
            final Response<T> execute = t.execute();
            return execute.body();
        } catch (IOException e) {
           throw new RuntimeException();
        }
    }

    default <T> void callAcyncMethod(Call<T> t) {
        t.enqueue(new Callback<T>() {
            @Override
            public void onResponse(Call<T> call, Response<T> response) {
                System.out.printf(response.body().toString());
            }

            @Override
            public void onFailure(Call<T> call, Throwable t) {

            }
        });
    }

    default Retrofit createApi(String url) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        return new Retrofit.Builder()
                //http://localhost:8081/second/hello/world/
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();
    }
}
