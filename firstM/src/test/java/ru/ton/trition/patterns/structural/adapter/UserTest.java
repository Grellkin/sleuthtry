package ru.ton.trition.patterns.structural.adapter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.ton.trition.patterns.structural.adapter.adapter.InternetPhoneAdapter;
import ru.ton.trition.patterns.structural.adapter.used_api.InternetUser;
import ru.ton.trition.patterns.structural.adapter.foreign_api.Phone;
import ru.ton.trition.patterns.structural.adapter.used_api.Internet;
import ru.ton.trition.patterns.structural.adapter.util.Resolver;

class UserTest {

    @Test
    public void callAmbulance() {
        String ipAddress = "1.1.1.1";
        Phone phone = new Phone();
        Resolver ipNumberResolver = new Resolver();
        Internet internet = new InternetPhoneAdapter(phone, ipNumberResolver);

        //you can`t pass phone to user, so pass adapter which redirect call from Internet to phone line
        //internetUser.call(phoneNumber) or call(phone) is impossible
        InternetUser internetUser = new InternetUser(internet);

        String data = internetUser.call(ipAddress);
        Assertions.assertEquals("hello, how can I help you?", data);
    }


}