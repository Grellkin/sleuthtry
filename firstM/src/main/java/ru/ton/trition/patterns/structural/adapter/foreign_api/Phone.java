package ru.ton.trition.patterns.structural.adapter.foreign_api;

public class Phone {

    public String makeCallToNumber(long number) {
        if (number == 1222L) {
            return "hello, how can I help you?";
        }
        return "this number is not available";
    }
}
