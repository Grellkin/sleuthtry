package ru.ton.trition.patterns.creational.abstract_factory.product;

public class RussianOwen extends Owen {
    public RussianOwen(double price, String country) {
        super(price, country);
    }
}
