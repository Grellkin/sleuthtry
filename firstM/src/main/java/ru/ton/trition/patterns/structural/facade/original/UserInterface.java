package ru.ton.trition.patterns.structural.facade.original;

import ru.ton.trition.patterns.structural.facade.facade.DbReaderPanel;

public class UserInterface {

    private DbReaderPanel panel;
    private DbAdminPanel apanel;

    public void setPanel(DbReaderPanel dbPanel) {
        this.panel = dbPanel;
        this.apanel = null;
    }


    public void setPanel(DbAdminPanel dbAPanel) {
        this.apanel = dbAPanel;
        this.panel = null;
    }

    public String readInfo() {
        if (panel != null) {
            return panel.readData();
        }
        if (apanel != null) {
            return apanel.readData();
        }
        throw new RuntimeException();
    }

    public void writeInfo(String privet) {
        if (apanel == null) {
            throw new RuntimeException();
        }
        apanel.writeData(privet);
    }
}
