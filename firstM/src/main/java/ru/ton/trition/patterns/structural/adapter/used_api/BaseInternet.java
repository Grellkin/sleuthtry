package ru.ton.trition.patterns.structural.adapter.used_api;

public class BaseInternet implements Internet {

    @Override
    public String callThroughTheInternet(String ipAddress) {
        return "this is info from ip-call";
    }

    @Override
    public void sendPicture(byte[] info) {
        System.out.println("Transfer info to the internet!");
    }

    @Override
    public void makeConnection() {
        System.out.println("you are in the internet now");
    }
}
