package ru.ton.trition.patterns.creational.abstract_factory.factory;

import ru.ton.trition.patterns.creational.abstract_factory.product.Kitchen;

public interface KitchenDesigner {

    Kitchen designNewKitchen();
}
