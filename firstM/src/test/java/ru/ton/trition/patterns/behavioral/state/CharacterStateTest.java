package ru.ton.trition.patterns.behavioral.state;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterStateTest {

    @Test
    public void testUser() {
        CharacterStateMachine character = new CharacterStateMachine();

        //alive
        character.moveForward();
        assertEquals(20, character.moveForward());
        assertEquals("ARRRRR For the King!", character.speak("For the King!"));
        character.dealDamage(10);
        character.dealDamage(15);
        character.receiveDamage(15);
        character.receiveDamage(150);

        //spirit
        assertEquals(45, character.moveForward());
        character.speak("For the King!");
        character.dealDamage(10);
        assertEquals(70, character.moveForward());
        character.receiveDamage(-10); //making char zombie

        //zombie
        assertEquals(73, character.moveForward());
        character.receiveDamage(7);
        assertEquals(75, character.moveForward());
        assertEquals("aaugh For the King! grhhh", character.speak("For the King!"));
        character.dealDamage(10);

    }

}