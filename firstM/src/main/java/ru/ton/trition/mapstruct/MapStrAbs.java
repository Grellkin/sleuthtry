package ru.ton.trition.mapstruct;

import org.springframework.beans.factory.annotation.Autowired;
import ru.ton.trition.mapstruct.source.Document;
import ru.ton.trition.mapstruct.source.Text;
import ru.ton.trition.mapstruct.target.Doc;
import ru.ton.trition.mapstruct.target.Tex;

//@Mapper(componentModel = "spring")

public abstract class MapStrAbs implements MapStr {

    @Autowired
    MapStr delegate;

    @Override
    public Doc convertDocumentToDoc(Document document) {
        final Doc doc = delegate.convertDocumentToDoc(document);
        doc.setKing("THE ALIANCE KING!");
        return doc;
    }


}
