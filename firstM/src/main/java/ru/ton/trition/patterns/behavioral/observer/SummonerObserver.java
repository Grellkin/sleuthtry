package ru.ton.trition.patterns.behavioral.observer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;

public class SummonerObserver implements Observer {

    private final EnumMap<Cast, List<NotifyTarget>> subscribers = new EnumMap<>(Cast.class);

    public SummonerObserver() {
        Arrays.stream(Cast.values()).forEach(cast -> subscribers.put(cast, new ArrayList<>()));
    }

    @Override
    public void subscribeToAction(Cast action, NotifyTarget target) {
        subscribers.get(action).add(target); //you can check here, that subscribing is unique
    }

    @Override
    public void simulateEvent(Cast event) {
         subscribers.get(event).forEach(t -> t.notifyMe(event));
    }

    @Override
    public void unsubscribeFromAllEvents(NotifyTarget target) {
        subscribers.values().forEach(list -> list.remove(target));
    }

    @Override
    public void unsubscribeFromEvent(Cast action, NotifyTarget target) {
        subscribers.get(action).remove(target);
    }
}
