package ru.ton.trition.patterns.structural.bridge.car_hirarchy;

import ru.ton.trition.patterns.structural.bridge.engine_hirarchy.Engine;
import ru.ton.trition.patterns.structural.bridge.transmission_hierarchy.Transmission;

public class Sedan extends Car {
    public Sedan(Engine engine, Transmission transmission) {
        super(engine, transmission);
    }

    @Override
    public void move() {
        if (engine.turnOn() && transmission.connectEngineAndWheels()) {
            System.out.println("Sedan is moving now!");
        } else {
            throw new RuntimeException("ITS BROKEN!");
        }
    }
}
