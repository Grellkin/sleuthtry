package ru.ton.trition.patterns.creational.factory_method.product;

public abstract class Walnut extends Nut {

    public Walnut(int size, String name, String origin) {
        super(size, name, origin);
    }

    @Override
    public int getNutrition() {
        return 11 * getKind().length();
    }

    protected abstract String getKind();
}
