package ru.ton.trition.patterns.behavioral.state;

import lombok.Data;

@Data
public class CharacterStateMachine {

    private CharacterState state = new AliveCharacterState(0, 100, this);

    public int moveForward() {
        return state.move();
    }

    public String speak(String s) {
        return state.say(s);
    }

    public void dealDamage(int i) {
        state.deal(i);
    }

    public void receiveDamage(int i) {
        state.receive(i);
    }

    //some methods doing something with state
}
