package ru.ton.trition.patterns.behavioral.strategy;

public class CookWorker extends Worker {

    private Education education;
    public CookWorker(int salary, Education education) {
        super(salary);
        this.education = education;
    }

    @Override
    public int makeMoneyByWork() {
        switch (education) {
            case HIGH:
                return 99;
            case LOW:
                return 55;
            case NEVER:
            default:
                return 14;
        }
    }
}
