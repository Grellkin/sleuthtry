package ru.ton.trition.patterns.structural.proxy;

public interface WorkerApi {

    double getSalary();
    void receivePayment(double money);
    double exposeAmoutOfMoney();
    void getSick(int days);
}
