package ru.ton.trition.patterns.structural.facade.facade;

import ru.ton.trition.patterns.structural.facade.original.Db;

public class DbReaderPanel {

    private final Db source;

    public DbReaderPanel(Db db) {
        this.source = db;
    }

    public String readData() {
        final String read = source.read();
        //maybe decode here, or change something
        return read;
    }

}
