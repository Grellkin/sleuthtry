package ru.ton.trition.retro;

import feign.Param;
import feign.RequestLine;
import ru.ton.triton.Retro;


public interface MananFeignAPI {

    @RequestLine("GET shalom/bruda?page={page}")
    Retro shalomBruda(@Param("page") int page);

    @RequestLine("POST shalom/bruda")
    String shalomBruda(Retro retro);

}
