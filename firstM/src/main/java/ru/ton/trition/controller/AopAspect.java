package ru.ton.trition.controller;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class AopAspect {

    /*
    execution(modifiers-pattern? ret-type-pattern declaring-type-pattern?name-pattern(param-pattern)
                throws-pattern?)
                https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#aop-pointcuts-examples
     */
    @Pointcut("execution(public * *(..))")
    void anyPublicMethod(){}

    @Pointcut("within(ru.ton.trition.controller.*)")
    void anyControllerMethod(){}

    @Before(value = "anyPublicMethod() && anyControllerMethod()")
    void advice() {
        System.out.println("Asalayam alyekum nahuy!");
    }

}
