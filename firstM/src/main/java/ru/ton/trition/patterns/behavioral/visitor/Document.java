package ru.ton.trition.patterns.behavioral.visitor;

public interface Document {
    void visit(Visitorio visitor);
    boolean checkValidity();
    void invalidate(String reason);
}
