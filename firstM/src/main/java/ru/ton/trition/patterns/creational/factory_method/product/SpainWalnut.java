package ru.ton.trition.patterns.creational.factory_method.product;

public class SpainWalnut extends Walnut {
    public SpainWalnut(int size, String name, String origin) {
        super(size, name, origin);
    }

    @Override
    protected String getKind() {
        return "si this is spain!";
    }
}
