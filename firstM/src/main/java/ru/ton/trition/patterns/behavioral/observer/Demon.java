package ru.ton.trition.patterns.behavioral.observer;

public class Demon extends NotifiableDemon {
    @Override
    public void notifyMe(Cast eventType) {
        if (Cast.DEMON_SUMMON.equals(eventType)) {
            summoned = true;
        } else {
            throw new RuntimeException("Something really wrong! Demon is messed up.");
        }
    }
}
