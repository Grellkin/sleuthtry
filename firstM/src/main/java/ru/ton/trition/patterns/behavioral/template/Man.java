package ru.ton.trition.patterns.behavioral.template;

public class Man extends Person {
    @Override
    protected void dieTemplate() {
        age += 25;
    }

    @Override
    protected void makeTeplateAction() {
        achievement = "served in army";
        System.out.println("achievement done! I am a MAN!");
    }
}
