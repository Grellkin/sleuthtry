package ru.ton.trition.patterns.creational.factory_method.factory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import ru.ton.trition.patterns.creational.factory_method.product.Nut;

@AllArgsConstructor
@Getter
@Setter
public abstract class NutTree {

    private int height = 0;
    private int age = 0;

    public abstract void grow();

    public void getOlder() {
        age += 1;
    }

    public abstract Nut produceNut();
}
