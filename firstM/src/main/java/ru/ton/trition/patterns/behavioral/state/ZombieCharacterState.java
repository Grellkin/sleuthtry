package ru.ton.trition.patterns.behavioral.state;

public class ZombieCharacterState extends AbsCharacterState {
    public ZombieCharacterState(int point, int health, CharacterStateMachine machine) {
        super(point, health, machine);
    }

    private int moveSpeed = 3;

    @Override
    public int move() {
        return (this.point += moveSpeed);
    }

    @Override
    public String say(String phrase) {
        return "aaugh " + phrase + " grhhh";
    }

    @Override
    public void deal(int damage) {
        System.out.println("zombie is pretty bad making damage :(");
    }

    @Override
    public void receive(int damage) {
        moveSpeed -= (damage % 3);
    }
}
