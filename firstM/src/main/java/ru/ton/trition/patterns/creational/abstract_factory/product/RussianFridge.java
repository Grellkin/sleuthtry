package ru.ton.trition.patterns.creational.abstract_factory.product;

public class RussianFridge extends Fridge {
    public RussianFridge(int size, String country) {
        super(size, country);
    }
}
