package ru.ton.trition.patterns.behavioral.strategy;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Restourant {

    //this is strategies, we change workers, we got different result of algorithm produceBenefit()
    private Worker chef;
    private Worker dish;

    public Restourant(Worker chef, Worker dishwasher) {
        this.chef = chef;
        this.dish = dishwasher;
    }

    public int produceBenefit() {
        return chef.makeMoneyByWork() + dish.makeMoneyByWork();
    }
}
