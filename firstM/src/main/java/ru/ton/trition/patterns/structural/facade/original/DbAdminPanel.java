package ru.ton.trition.patterns.structural.facade.original;

import java.nio.charset.StandardCharsets;

public class DbAdminPanel {
    private final Db source;

    public DbAdminPanel(Db db) {
        this.source = db;
    }

    public String readData() {
        return source.read();
    }

    public void writeData(String data) {
        source.write(data.getBytes(StandardCharsets.UTF_8));
    }
}
