package ru.ton.trition.patterns.structural.adapter.adapter;

import ru.ton.trition.patterns.structural.adapter.util.Resolver;
import ru.ton.trition.patterns.structural.adapter.foreign_api.Phone;
import ru.ton.trition.patterns.structural.adapter.used_api.Internet;

public class InternetPhoneAdapter implements Internet {

    private Phone phone;
    private Resolver resolver;

    public InternetPhoneAdapter(Phone phone, Resolver ipNumberResolver) {
        this.phone = phone;
        this.resolver = ipNumberResolver;
    }

    @Override
    public String callThroughTheInternet(String ipAddress) {
        final long phoneNumber = resolver.makeResolutionFromIpToPhoneNumber(ipAddress);
        return phone.makeCallToNumber(phoneNumber);
    }

    @Override
    public void sendPicture(byte[] info) {
        throw new RuntimeException("Our phones does not work with pictures!");
    }

    @Override
    public void makeConnection() {
        System.out.println("shshshhshpek, internet connected to phone lines!");
    }
}
