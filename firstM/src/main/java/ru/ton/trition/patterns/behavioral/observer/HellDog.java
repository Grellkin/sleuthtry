package ru.ton.trition.patterns.behavioral.observer;

public class HellDog extends NotifiableDemon {
    @Override
    public void notifyMe(Cast eventType) {
        if (Cast.HELLDOG_SUMMON.equals(eventType)) {
            summoned = true;
        } else {
            System.out.println("Really bad, dog is angry, why you bother me?");
        }
    }
}
