package ru.ton.trition.patterns.behavioral.observer;

public interface Observer {

    void subscribeToAction(Cast action, NotifyTarget target);

    void simulateEvent(Cast event);

    void unsubscribeFromAllEvents(NotifyTarget target);

    void unsubscribeFromEvent(Cast action, NotifyTarget target);

}
