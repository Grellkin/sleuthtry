package ru.ton.trition.patterns.behavioral.state;

public abstract class AbsCharacterState implements CharacterState {

    protected int point;
    protected int health;
    protected CharacterStateMachine machine;

    public AbsCharacterState(int point, int health, CharacterStateMachine machine) {
        this.point = point;
        this.health = health;
        this.machine = machine;
    }
}
