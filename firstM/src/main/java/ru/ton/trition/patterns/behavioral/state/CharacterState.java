package ru.ton.trition.patterns.behavioral.state;

public interface CharacterState {

    int move();

    String say(String phrase);

    void deal(int damage);

    void receive(int damage);
}
