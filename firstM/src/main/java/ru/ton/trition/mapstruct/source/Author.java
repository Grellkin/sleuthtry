package ru.ton.trition.mapstruct.source;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
public class Author {

    private String name;
}
