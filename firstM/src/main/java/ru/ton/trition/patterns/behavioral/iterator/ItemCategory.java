package ru.ton.trition.patterns.behavioral.iterator;

public enum ItemCategory {
    FOOD, SPORT, CLOTH, DEVICES
}
