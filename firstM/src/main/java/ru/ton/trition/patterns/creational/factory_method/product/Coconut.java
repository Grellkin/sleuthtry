package ru.ton.trition.patterns.creational.factory_method.product;


public class Coconut extends Nut {

    private static final int NUTRITION_PER_SIZE = 25;

    public Coconut(int size, String name, String origin) {
        super(size, name, origin);
    }

    @Override
    public int getNutrition() {
        return getSize() * NUTRITION_PER_SIZE;
    }

}
