package ru.ton.trition.patterns.behavioral.state;

public class DeadCharacterState extends AbsCharacterState {
    public DeadCharacterState(int point, int health, CharacterStateMachine machine) {
        super(point, health, machine);
    }

    @Override
    public int move() {
        return (this.point += 25);
    }

    @Override
    public String say(String phrase) {
        return "You cant speak while you dead.";
    }

    @Override
    public void deal(int damage) {
        //do nothing
    }

    @Override
    public void receive(int damage) {
        //damage make dead char a zombie
        machine.setState(new ZombieCharacterState(point, 0, this.machine));
    }
}
