package ru.ton.trition.model;

import lombok.Data;

@Data
public class Cat extends Animal<Cat> {

    private String breed;

    @Override
    public int subcompare(Cat animal) {
        return 0;
    }

    @Override
    public int compareTo(Cat o) {
        return 0;
    }
}
