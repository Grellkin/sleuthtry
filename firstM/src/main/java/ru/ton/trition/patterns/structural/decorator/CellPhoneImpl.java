package ru.ton.trition.patterns.structural.decorator;

public class CellPhoneImpl implements CellPhone {
    @Override
    public String call() {
        return "Hello, it`s John cena, how are you?";
    }

    @Override
    public void sendSms(String sms) {
        System.out.println("Send sms to address: " + sms);
    }
}
