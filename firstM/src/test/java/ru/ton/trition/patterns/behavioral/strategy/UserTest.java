package ru.ton.trition.patterns.behavioral.strategy;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    public void testUser() {

        Worker chef = new CookWorker(155, Worker.Education.HIGH);
        Worker dishwasher = new DishWorker(100);

        Worker crappyChef = new CookWorker(120, Worker.Education.LOW);
        Worker bestDishwasher = new DishWorker(140);

        Restourant restourant = new Restourant(chef, dishwasher);
        int dollars = restourant.produceBenefit();
        restourant.setChef(crappyChef);
        int dollarsWithNewChef = restourant.produceBenefit();
        restourant.setDish(bestDishwasher);
        int dollarsWithNewDisher = restourant.produceBenefit();
        assertTrue(dollars > dollarsWithNewChef);
        assertTrue(dollarsWithNewDisher > dollarsWithNewChef);
    }

}