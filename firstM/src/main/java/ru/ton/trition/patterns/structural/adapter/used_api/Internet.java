package ru.ton.trition.patterns.structural.adapter.used_api;

public interface Internet {

    String callThroughTheInternet(String ipAddress);

    void sendPicture(byte[] info);

    void makeConnection();
}
