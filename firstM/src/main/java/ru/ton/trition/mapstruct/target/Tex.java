package ru.ton.trition.mapstruct.target;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@AllArgsConstructor
public class Tex {

    private String footage;
    private String body;
    @Setter
    private int priceOfTex;
}
