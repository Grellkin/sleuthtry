package ru.ton.trition.patterns.structural.adapter.used_api;

import ru.ton.trition.patterns.structural.adapter.used_api.Internet;

public class InternetUser {

    private Internet internet;

    public InternetUser(Internet internet) {
        this.internet = internet;
    }

    public String call(String ipAddress) {
        internet.makeConnection();
        return internet.callThroughTheInternet(ipAddress);
    }


}
