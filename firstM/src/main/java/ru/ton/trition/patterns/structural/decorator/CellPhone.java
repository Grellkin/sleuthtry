package ru.ton.trition.patterns.structural.decorator;

public interface CellPhone {

    String call();
    void sendSms(String sms);
}
