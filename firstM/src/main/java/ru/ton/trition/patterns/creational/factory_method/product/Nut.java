package ru.ton.trition.patterns.creational.factory_method.product;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public abstract class Nut {

    private final int size;
    private final String name;
    private final String origin;

    public abstract int getNutrition();
}
