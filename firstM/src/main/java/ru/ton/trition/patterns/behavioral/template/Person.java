package ru.ton.trition.patterns.behavioral.template;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Person {

    protected int age;
    protected String achievement;

    public void liveDecentLife() {
        growUp();
        makeTeplateAction();
        makeSpouse();
        dieTemplate();
    }

    protected abstract void dieTemplate();

    protected void makeSpouse() {
        age += 4;
        System.out.println("I made a spouse!");
    }

    protected abstract void makeTeplateAction();

    private void growUp() {
        age += 18;
        System.out.println("I grew up!");
    }
}
