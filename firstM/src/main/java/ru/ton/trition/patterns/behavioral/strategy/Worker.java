package ru.ton.trition.patterns.behavioral.strategy;

public abstract class Worker {

    protected int salary;

    public Worker(int salary) {
        this.salary = salary;
    }

    public int makeMoneyByWork() {
        return 10;
    }

    protected enum Education {
        HIGH, LOW, NEVER
    }
}
