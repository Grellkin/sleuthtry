package ru.ton.triton;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecondMApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecondMApplication.class, args);
    }

}
