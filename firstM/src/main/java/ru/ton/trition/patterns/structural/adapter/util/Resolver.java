package ru.ton.trition.patterns.structural.adapter.util;

public class Resolver {

    public long makeResolutionFromIpToPhoneNumber(String ip) {
        switch (ip) {
            case "1.1.1.1":
                return 1222L;
            case "12.12.12.12":
                return 1211L;
            default:
                return 8800L;
        }
    }
}
