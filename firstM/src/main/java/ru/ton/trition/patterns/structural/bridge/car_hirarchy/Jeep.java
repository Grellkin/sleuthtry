package ru.ton.trition.patterns.structural.bridge.car_hirarchy;

import ru.ton.trition.patterns.structural.bridge.engine_hirarchy.Engine;
import ru.ton.trition.patterns.structural.bridge.transmission_hierarchy.Transmission;

public class Jeep extends Car {
    public Jeep(Engine engine, Transmission transmission) {
        super(engine, transmission);
    }

    @Override
    public void move() {
        if (engine.turnOn() && transmission.connectEngineAndWheels()) {
            System.out.println("Jeep is moving now!");
        } else {
            throw new RuntimeException("JEEP is BROKEN!");
        }
    }
}
