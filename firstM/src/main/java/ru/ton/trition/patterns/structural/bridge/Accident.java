package ru.ton.trition.patterns.structural.bridge;

import lombok.Getter;
import ru.ton.trition.patterns.structural.bridge.car_hirarchy.Car;
import ru.ton.trition.patterns.structural.bridge.engine_hirarchy.BrokenEngine;
import ru.ton.trition.patterns.structural.bridge.transmission_hierarchy.BrokenTransmission;

@Getter
public class Accident {

    private Car firstParticipant;
    private Car secondParticipant;

    public Accident(Car car, Car oncomingCar) {
        car.setEngine(new BrokenEngine());
        oncomingCar.setTransmission(new BrokenTransmission());
        this.firstParticipant = car;
        this.secondParticipant = oncomingCar;
    }

}
