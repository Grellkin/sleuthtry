package ru.ton.trition.patterns.structural.proxy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    public void testUser() {
        /*
        Important difference between proxy and decorator is a living cycle of decorated/proxied object,
            usually user has access to original object and to decorated in case of decoration also decorator is able
            to add few own methods, from the other hand proxy guarantee no access to original object for clients and
            carry exact same interface.
        Wrapped object always passed to decorator class, but not the same for proxy. Sometimes proxy has authority to
            create instance by herself or by a part of dependency injection.
         */


        WorkerApi worker = new LegalWorker(new Worker()); //or new LegalWorker and proxy create instance by herself
        Assertions.assertEquals(0.0, worker.exposeAmoutOfMoney());
        double money = worker.getSalary();
        worker.receivePayment(money);
        Assertions.assertEquals(worker.getSalary() * 0.8, worker.exposeAmoutOfMoney());
        int days = 4;
        worker.getSick(days);
        Assertions.assertEquals(money + days * worker.getSalary() / 30, worker.exposeAmoutOfMoney());

    }

}