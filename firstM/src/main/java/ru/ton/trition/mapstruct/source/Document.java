package ru.ton.trition.mapstruct.source;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@Getter
public class Document {

    private int age;
    private String name;
    private Author author;
    private List<Page> pages;

}
