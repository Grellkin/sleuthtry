package ru.ton.trition.patterns.creational.abstract_factory.product;

import lombok.AllArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@ToString
public abstract class Owen {

    private double price;
    private String country;


}
