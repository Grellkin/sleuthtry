package ru.ton.trition.patterns.behavioral.visitor;

public class PrinterVisitor implements Visitorio {

    private String result;

    @Override
    public void printInfoAboutPersonalDoc(PersonalDocument personalDocument) {
        StringBuilder builder = new StringBuilder();
        builder.append("This is ");
        builder.append(personalDocument.getName());
        builder.append(" passport, his last name is ");
        builder.append(personalDocument.getLastName());
        builder.append(", his age is ");
        builder.append(personalDocument.getAge());
        result = builder.toString();
    }

    @Override
    public void printInfoAboutDealDoc(DealDocument dealDocument) {
        StringBuilder builder = new StringBuilder();
        builder.append("Civilian ");
        builder.append(dealDocument.getNameSeller());
        builder.append(" sold car to civilian ");
        builder.append(dealDocument.getNameBuyer());
        result = builder.toString();
    }

    @Override
    public String getVisitResult() {
        return result;
    }
}
