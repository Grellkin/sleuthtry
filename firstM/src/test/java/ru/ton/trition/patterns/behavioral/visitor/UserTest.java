package ru.ton.trition.patterns.behavioral.visitor;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserTest {

    @Test
    public void testUser() {
        Document personalDoc = new PersonalDocument("Name", "LastName", 19);
        Document dealDoc = new DealDocument("Name_seller", "LastName_seller",
                "Name_buyer", "Last_name_buyer");

        //we can add more visitors, any style, any functions which we do not wanna add directly to document classes
        Visitorio visitorio = new PrinterVisitor();

        personalDoc.visit(visitorio);
        assertEquals("This is Name passport, his last name is LastName, his age is 19", visitorio.getVisitResult());

        dealDoc.visit(visitorio);
        assertEquals("Civilian Name_seller sold car to civilian Name_buyer", visitorio.getVisitResult());
    }

}