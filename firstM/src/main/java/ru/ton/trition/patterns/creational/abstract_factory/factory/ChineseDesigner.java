package ru.ton.trition.patterns.creational.abstract_factory.factory;

import ru.ton.trition.patterns.creational.abstract_factory.product.ChineseFridge;
import ru.ton.trition.patterns.creational.abstract_factory.product.ChineseOwen;
import ru.ton.trition.patterns.creational.abstract_factory.product.Fridge;
import ru.ton.trition.patterns.creational.abstract_factory.product.Owen;

public class ChineseDesigner extends KitchenDesignerImpl {
    @Override
    protected Owen createOwen() {
        return new ChineseOwen(12, "CHINEY");
    }

    @Override
    protected Fridge createFridge() {
        return new ChineseFridge(11, "CHINEY");
    }
}
