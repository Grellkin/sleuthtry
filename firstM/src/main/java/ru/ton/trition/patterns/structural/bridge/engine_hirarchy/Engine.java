package ru.ton.trition.patterns.structural.bridge.engine_hirarchy;

public interface Engine {

    boolean turnOn();
    void makeSound();
}
