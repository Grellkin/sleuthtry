package ru.ton.trition.model;

import lombok.Data;

@Data
public abstract class Animal<T extends Animal<T>> implements Comparable<T>{

    private Integer age;

    public abstract int subcompare(T animal);
}
