package ru.ton.trition.patterns.structural.bridge.engine_hirarchy;

public class StrongEngine implements Engine {
    @Override
    public boolean turnOn() {
        return true;
    }

    @Override
    public void makeSound() {
        System.out.println("WRUUUM!! MAFAKA");
    }
}
