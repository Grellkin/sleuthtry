package ru.ton.trition.patterns.behavioral.visitor;

public interface Visitorio {

    void printInfoAboutPersonalDoc(PersonalDocument personalDocument);
    void printInfoAboutDealDoc(DealDocument dealDocument);

    String getVisitResult();
}
