package ru.ton.trition.patterns.behavioral.strategy;

public class DishWorker extends Worker {
    public DishWorker(int i) {
        super(i);
    }

    @Override
    public int makeMoneyByWork() {
        return salary / 5;
    }
}
