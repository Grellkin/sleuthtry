package ru.ton.trition.patterns.structural.proxy;

public class LegalWorker implements WorkerApi {

    private final static double TAX_RATE = 0.8;

    private Worker worker;

    public LegalWorker(Worker worker) {
        this.worker = worker;
    }

    @Override
    public double getSalary() {
        return worker.getSalary();
    }

    @Override
    public void receivePayment(double money) {
        worker.receivePayment(money * TAX_RATE);
    }

    @Override
    public double exposeAmoutOfMoney() {
        return worker.exposeAmoutOfMoney();
    }

    @Override
    public void getSick(int days) {
        worker.getSick(days);
        worker.receivePayment(worker.getSalary() * days / 30);
    }
}
