package ru.ton.trition.patterns.structural.bridge;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.ton.trition.patterns.structural.bridge.car_hirarchy.Car;
import ru.ton.trition.patterns.structural.bridge.car_hirarchy.Jeep;
import ru.ton.trition.patterns.structural.bridge.car_hirarchy.Sedan;
import ru.ton.trition.patterns.structural.bridge.engine_hirarchy.DefaultEngine;
import ru.ton.trition.patterns.structural.bridge.engine_hirarchy.Engine;
import ru.ton.trition.patterns.structural.bridge.engine_hirarchy.StrongEngine;
import ru.ton.trition.patterns.structural.bridge.transmission_hierarchy.DefaultTransmission;
import ru.ton.trition.patterns.structural.bridge.transmission_hierarchy.PowerfulTransmission;
import ru.ton.trition.patterns.structural.bridge.transmission_hierarchy.Transmission;

public class UserTest {

    @Test
    public void testUser() {
        Engine engine = new DefaultEngine();
        Engine powerfulEngine = new StrongEngine();

        Transmission transmission = new DefaultTransmission();
        Transmission powerfulTransmission = new PowerfulTransmission();

        Car car = new Sedan(engine, transmission);
        Car oncomingCar = new Jeep(powerfulEngine, powerfulTransmission);
        Car parkedCar = new Sedan(engine, powerfulTransmission);

        car.move();
        oncomingCar.move();
        Accident accident = new Accident(car, oncomingCar);

        Assertions.assertThrows( RuntimeException.class, car::move);
        Assertions.assertThrows( RuntimeException.class, oncomingCar::move);

        RepairSaloon saloon = new RepairSaloon();
        saloon.setDonorCar(parkedCar);
        saloon.fixCar(car);
        saloon.fixCar(oncomingCar);

        Assertions.assertDoesNotThrow(car::move);
        Assertions.assertDoesNotThrow(oncomingCar::move);
    }
}
