package ru.ton.trition.patterns.creational.factory_method.factory;

import ru.ton.trition.patterns.creational.factory_method.product.GreeceWalnut;
import ru.ton.trition.patterns.creational.factory_method.product.Nut;
import ru.ton.trition.patterns.creational.factory_method.product.SpainWalnut;

public class WalnutPalm extends NutTree {

    private String country;

    public WalnutPalm(int height, int age, String country) {
        super(height, age);
        this.country = country;
    }

    @Override
    public void grow() {
        System.out.println("walnut palm not grow, reject!");
    }

    @Override
    public Nut produceNut() {
        if (country.equals("greece")) {
            return new GreeceWalnut(2, "greca", "GRECA!");
        }
        return new SpainWalnut(3, "better greca", "SPAIN!");
    }
}
