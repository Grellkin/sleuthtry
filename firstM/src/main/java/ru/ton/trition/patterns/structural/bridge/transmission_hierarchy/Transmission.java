package ru.ton.trition.patterns.structural.bridge.transmission_hierarchy;

public interface Transmission {

    boolean connectEngineAndWheels();
}
